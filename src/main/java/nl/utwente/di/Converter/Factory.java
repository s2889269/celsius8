package nl.utwente.di.Converter;


public class Factory {

    public double getBookPrice(String degree){
        int x = Integer.parseInt(degree);
        return (x * (9.0/5.0)) + 32.0;
    }
}